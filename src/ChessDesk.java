/*
 *Yaroslav Zhovnovatyuk
 *
 * Copyright (c) 1983-2018 Yaroslav Zhovnovatyuk, All rights reserved
 */
import java.util.Arrays;

/**
 * This class made for training. It fills strings with symbols
 * in the chess order.
 *
 * @version    1.0 Nov 2018
 * @author     Yaroslav Zhovnovatyuk
 */
public class ChessDesk {

    public static void main(String[] args) {
        /** Variable i describes quantity of columns in the chess field */
        int i = 10;

        /** Variable j describes quantity of rows in the chess field */
        int j = 8;

        /** Variable c1 describes first symbol composing chess field */
        char c1 = 'B';

        /** Variable c2 describes second symbol composing chess field */
        char c2 = 'W';

        /** Chess field filling and output*/
        for (int k = 0; k < j; k++) {
            char cf1 = c1;
            char cf2 = c2;
            if (k %2 == 0) {
                cf1 = c2;
                cf2 = c1;
            }
            System.out.println(row(i, cf1, cf2));
        }
    }

    /** Method row makes a string of alternating symbols
     * @param i describes quantity of signs in the row
     * @param c1 describes first alternating symbol
     * @param c2 describes second alternating symbol
     */
    private static String row(int i, char c1, char c2) {
        char [] str = new char [i];
        Arrays.fill(str, c1);
        for (int j = 0; j < i; j++) {
            if (j % 2 == 0) {
                str[j] = c2;
            }
        }
        return new String(str);
    }
}
